#ifndef ENDIANHLP_H
#define ENDIANHLP_H

#include <cstdint>

#define L_ORDER_LITTLE_ENDIAN  1
#define L_ORDER_BIG_ENDIAN     2

#if defined(__GNUC__)
    #if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        #define L_BYTE_ORDER L_ORDER_LITTLE_ENDIAN
    #elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
        #define L_BYTE_ORDER L_ORDER_BIG_ENDIAN
    #endif
#endif

#if !defined(L_BYTE_ORDER)
#error Endianness not defined
#endif

template<typename T>
T reverse(T t)
{
    T res;
    uint8_t *pdst = (uint8_t*)&res;
    uint8_t *psrc = (uint8_t*)&t + sizeof(T) - 1;
    for (size_t i = 0; i < sizeof(T); ++i, ++pdst, --psrc)
    {
        *pdst = *psrc;
    }
    return res;
}

template<typename T>
struct ReverseBytes
{
    T t;
    ReverseBytes(T t = {}) : t(reverse(t)) {}
    operator T() const { return reverse(t); }
};

#if L_BYTE_ORDER == L_ORDER_LITTLE_ENDIAN 
using be_uint16_t = ReverseBytes<uint16_t>;
using be_uint32_t = ReverseBytes<uint32_t>;
using be_uint64_t = ReverseBytes<uint64_t>;
using le_uint16_t = uint16_t;
using le_uint32_t = uint32_t;
using le_uint64_t = uint64_t;
#elif L_BYTE_ORDER == L_ORDER_BIG_ENDIAN 
using be_uint16_t = uint16_t;
using be_uint32_t = uint32_t;
using be_uint64_t = uint64_t;
using le_uint16_t = ReverseBytes<uint16_t>;
using le_uint32_t = ReverseBytes<uint32_t>;
using le_uint64_t = ReverseBytes<uint64_t>;
#endif

#endif // defined ENDIANHLP_H

