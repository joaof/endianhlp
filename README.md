endianhlp
=========

Simple integer type definitions for reading/writing little or big endian values transparently.

C++11. Currently only for GCC.
