#include <iostream>
#include <cppunit/TestAssert.h>

#include "endianhlp.h"

template<typename T>
struct UTest
{
    union
    {
        T       val;
        uint8_t bytes[sizeof(T)];
    };
    UTest() : val(0) {}
};

namespace CppUnit 
{
    template<>
    struct assertion_traits<uint8_t>
    {
        static bool equal(const uint8_t& x, const uint8_t& y )
        {
            return x == y;
        }

        static std::string toString( const uint8_t& x )
        {
            OStringStream ost;
            ost << std::hex << (uint16_t)x;
            return ost.str();
        }
    };
}

int main()
{

    {
        UTest<le_uint16_t> le16;
        le16.val = 0x1234;
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x34, le16.bytes[0]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x12, le16.bytes[1]);
        CPPUNIT_ASSERT(le16.val == 0x1234);
    }

    {
        UTest<le_uint32_t> le32;
        le32.val = 0x12345678;
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x78, le32.bytes[0]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x56, le32.bytes[1]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x34, le32.bytes[2]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x12, le32.bytes[3]);
        CPPUNIT_ASSERT(le32.val == 0x12345678);
    }

    {
        UTest<le_uint64_t> le64;
        le64.val = 0x123456789ABCDEF0;
        CPPUNIT_ASSERT_EQUAL((uint8_t)0xF0, le64.bytes[0]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0xDE, le64.bytes[1]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0xBC, le64.bytes[2]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x9A, le64.bytes[3]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x78, le64.bytes[4]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x56, le64.bytes[5]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x34, le64.bytes[6]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x12, le64.bytes[7]);
        CPPUNIT_ASSERT(le64.val == 0x123456789ABCDEF0);
    }

    {
        UTest<be_uint16_t> be16;
        be16.val = 0x1234;
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x12, be16.bytes[0]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x34, be16.bytes[1]);
        CPPUNIT_ASSERT(be16.val == 0x1234);
    }

    {
        UTest<be_uint32_t> be32;
        be32.val = 0x12345678;
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x12, be32.bytes[0]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x34, be32.bytes[1]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x56, be32.bytes[2]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x78, be32.bytes[3]);
        CPPUNIT_ASSERT(be32.val == 0x12345678);
    }

    {
        UTest<be_uint64_t> be64;
        be64.val = 0x123456789ABCDEF0;
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x12, be64.bytes[0]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x34, be64.bytes[1]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x56, be64.bytes[2]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x78, be64.bytes[3]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0x9A, be64.bytes[4]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0xBC, be64.bytes[5]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0xDE, be64.bytes[6]);
        CPPUNIT_ASSERT_EQUAL((uint8_t)0xF0, be64.bytes[7]);
        CPPUNIT_ASSERT(be64.val == 0x123456789ABCDEF0);
    }

}

